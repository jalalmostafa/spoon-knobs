package com.jalal.knobs;

import spoon.Launcher;

public class Main {

    public  static void main(String[] args) {
        try {
            Launcher launcher = new Launcher();
            launcher.addInputResource(args[0]);
            launcher.addProcessor(new AssignmentsProcessor());
            launcher.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
