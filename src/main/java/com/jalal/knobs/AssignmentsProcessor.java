package com.jalal.knobs;

import spoon.processing.AbstractProcessor;
import spoon.reflect.code.*;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtElement;
import spoon.reflect.declaration.CtField;
import spoon.reflect.declaration.ModifierKind;
import spoon.reflect.reference.CtFieldReference;
import spoon.reflect.reference.CtVariableReference;
import spoon.reflect.visitor.Filter;

import java.util.List;
import java.util.UUID;

public class AssignmentsProcessor extends AbstractProcessor<CtAssignment<Object, Object>> {

    @Override
    public boolean isToBeProcessed(CtAssignment<Object, Object> assignment) {
        CtExpression<Object> expr = assignment.getAssignment();
        return isPrimary(expr);
    }

    public void process(CtAssignment<Object, Object> assignment) {
        CtClass ctClass = (CtClass) assignment.getParent(new Filter<CtElement>() {
            public boolean matches(CtElement ctElement) {
                return ctElement instanceof CtClass;
            }
        });
        CtField field = getFactory().Code().createCtField(fieldName(), assignment.getType(), assignment.getAssignment().toString(), ModifierKind.PRIVATE);
        ctClass.addField(field);
        CtExpression assignmentExpr = getFactory().Code().createVariableRead(field.getReference(), false);
        assignment.setAssignment(assignmentExpr);
    }

    private String fieldName() {
        String name = UUID.randomUUID().toString().replace("-", "");
        return "_generated_" + name;
    }

    private boolean isPrimary(CtExpression expr) {
        if (expr instanceof CtLiteral
                || expr instanceof CtNewArray) {
            return true;
        }

        if (expr instanceof CtBinaryOperator) {
            CtBinaryOperator bin = (CtBinaryOperator) expr;
            // only if both operands can be processed
            return isPrimary(bin.getLeftHandOperand()) && isPrimary(bin.getRightHandOperand());
        }

        if (expr instanceof CtUnaryOperator) {
            CtUnaryOperator unary = (CtUnaryOperator) expr;
            return isPrimary(unary.getOperand()); // only if the operand can be processed
        }

        if (expr instanceof CtConstructorCall) {
            CtConstructorCall call = (CtConstructorCall) expr;
            call.getArguments();
            boolean primaryArguments = true;
            List<CtExpression> arguments = call.getArguments();
            for (CtExpression ex : arguments) {
                primaryArguments = primaryArguments && isPrimary(ex);
            }
            return primaryArguments; // only if all elements can be processed
        }

        if (expr instanceof CtArrayRead) {
            CtArrayRead arrayRead = (CtArrayRead) expr;
            CtVariableReference ref = ((CtVariableRead) arrayRead.getTarget()).getVariable();
            return (ref instanceof CtFieldReference); // only if the array can be processed
        }

        return false;
    }
}
